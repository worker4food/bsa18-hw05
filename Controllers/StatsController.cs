using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Services;
using Models;

namespace Controllers
{
    public class StatsController : Controller
    {
        private BlogData repo;

        public StatsController(BlogData _repo) {
            repo = _repo;
        }

        public IActionResult UserPostsCommentsCount(long userId = -1)
        {
            if(userId < 0)
                return View("SelectUser", repo.Users);
            else
                return View(repo.UserPostsCommentsCount(userId));
        }

        public IActionResult ShortUserComments(long userId = -1)
        {
            if(userId < 0)
                return View("SelectUser", repo.Users);
            else
                return View(repo.ShortUserComments(userId));
        }

        public IActionResult CompletedUserTodos(long userId = -1)
        {
            if(userId < 0)
                return View("SelectUser", repo.Users);
            else
                return View(repo.CompletedUserTodos(userId));
        }

        public IActionResult AllUsersTodos()
        {
            return View(repo.AllUsersTodos());
        }

        public IActionResult UserStatistics(long userId = -1)
        {
            if(userId < 0)
                return View("SelectUser", repo.Users);
            else
                return View(repo.UserStatistics(userId));
        }

        public IActionResult PostStatistics(long postId = -1)
        {
            if(postId < 0)
                return View("SelectPost", repo.Posts);
            else
                return View(repo.PostStatistics(postId));
        }
    }
}
