﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Models;
using Services;

namespace Controllers
{
    public class HomeController : Controller
    {
        protected BlogData repo;

        public HomeController(BlogData _repo) {
            repo = _repo;
        }

        public IActionResult Index()
        {
            var postLikes =
                from p in repo.Posts
                group p by p.User into ul
                select (User: ul.Key, Likes: ul.Sum(up => up.Likes));

            var dashboard = new Dashboard {
                TopPosts = repo.Posts.OrderByDescending(p => p.Likes).Take(10),
                RecentTodos = repo.Todos.OrderByDescending(t => t.CreatedAt).Take(10),
                TopUsers = postLikes.OrderByDescending(pl => pl.Likes).Take(10)
            };

            return View(dashboard);
        }
    }
}
