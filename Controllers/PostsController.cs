using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using Models;

namespace Controllers
{
    public class PostsController : Controller
    {
        private BlogData repo;

        public PostsController(BlogData _repo) {
            repo = _repo;
        }

        public ActionResult Index(long userId = -1)
        {
            var vm = repo.Posts;

            if(userId > 0)
                vm = vm.Where(p => p.UserId == userId);

            return View(vm);
        }

        // GET: Post/Details/5
        public ActionResult Details(int id)
        {
            Post p;

            if(repo.PostSet.TryGetValue(id, out p))
                return View(p);
            else
                return new NotFoundResult();
        }
    }
}
