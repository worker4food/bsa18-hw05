using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using Models;

namespace Controllers
{
    public class UsersController : Controller
    {
        private BlogData repo;

        public UsersController(BlogData _repo) {
            repo = _repo;
        }

        // GET: Users
        public ActionResult Index()
        {
            return View(repo.Users);
        }

        // GET: Users/Details/5
        public ActionResult Details(int id)
        {
            User u;
            if(!repo.UserSet.TryGetValue(id, out u))
                return new NotFoundResult();
                //($"User with id {id} not found");
            
            return View(u);
        }
    }
}
