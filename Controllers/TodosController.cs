using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Services;
using Models;

namespace Controllers
{
    public class TodosController : Controller
    {
        private BlogData repo;

        public TodosController(BlogData _repo) {
            repo = _repo;
        }

        // GET: Todos
        public ActionResult Index(long userId = -1)
        {
            var ts = repo.Todos;

            if(userId > 0)
                ts = ts.Where(t => t.UserId == userId);

            return View(ts);
        }

        // GET: Todos/Details/5
        public ActionResult Details(int id)
        {
            ToDo res = repo.Todos.Where(t => t.Id == id).FirstOrDefault();
            if(res != null)
                return View(res);
            else
                return new NotFoundResult();
        }
    }
}
