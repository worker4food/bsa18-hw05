using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using Models;

namespace Services
{
    public class BlogData {
        private static readonly string baseUrl = "http://5b128555d50a5c0014ef1204.mockapi.io";
        private static readonly string countryUrl = "https://raw.githubusercontent.com/lukes/ISO-3166-Countries-with-Regional-Codes/v6.0/slim-2/slim-2.json";

        public IDictionary<long, User> UserSet { get; private set; }
        public IDictionary<long, Post> PostSet { get; private set; }
        public IDictionary<string, Country> CountrySet { get; private set; }

        public  IEnumerable<User> Users { get; private set; }
        public IEnumerable<Post> Posts { get; private set; }
        public IEnumerable<ToDo> Todos { get; private set; }
        
        protected IEnumerable<Comment> Comments { get; private set; }
        public IEnumerable<Address> Addresses { get; private set; }

        private BlogData() {}

        public static BlogData CreateInstance(
                IEnumerable<User> users,
                IEnumerable<Post> posts,
                IEnumerable<Comment> comments,
                IEnumerable<ToDo> todos,
                IEnumerable<Address> addresses,
                IEnumerable<Country> countries) {

            var data = new BlogData();

            //Fill comments with User and Post
            data.Comments =
                from c in comments
                join u in users on c.UserId equals u.Id
                join p in posts on c.PostId equals p.Id
                select c.withUser(u).withPost(p);

            //Fill addresses with User
            data.Addresses =
                from a in addresses
                join u in users on a.UserId equals u.Id
                select a.withUser(u);

            //Fill todos with User
            data.Todos =
                from t in todos
                join u in users on t.UserId equals u.Id
                select t.withUser(u);

            //Fill posts with User and related Comment[]
            data.Posts =
                from p in posts
                join u in users on p.UserId equals u.Id
                join c in data.Comments on p.Id equals c.PostId into pcs
                select p.withUser(u).withComments(pcs);

            //Construct root (User[]) object with related Post[] and Comment[]
            data.Users =
                from u in users
                join p in data.Posts on u.Id equals p.UserId into ups
                join c in data.Comments on u.Id equals c.UserId into ucs
                join t in data.Todos on u.Id equals t.UserId into uts
                join a in data.Addresses on u.Id equals a.UserId into uas
                let ua = uas.FirstOrDefault() 
                select u.withPosts(ups).withComments(ucs).withTodos(uts).witAddress(ua);

            data.UserSet = data.Users.ToDictionary(u => u.Id);
            data.PostSet = data.Posts.ToDictionary(p => p.Id);
            data.CountrySet = countries.ToDictionary(c => c.Name);

            return data;
        }

        public static async Task<BlogData> CreateInstanceAsync() {

            using(var client = new HttpClient()) {
                var usersTask = fetchObjects<User>(client, $"{baseUrl}/users");
                var postsTask = fetchObjects<Post>(client, $"{baseUrl}/posts");
                var commentsTask = fetchObjects<Comment>(client, $"{baseUrl}/comments");
                var todosTask = fetchObjects<ToDo>(client, $"{baseUrl}/todos");
                var addressesTask = fetchObjects<Address>(client, $"{baseUrl}/address");
                var countriesTask = fetchObjects<Country>(client, countryUrl);

                await Task.WhenAll(usersTask, postsTask, commentsTask, todosTask, addressesTask, countriesTask);

                return CreateInstance(
                    await usersTask,
                    await postsTask,
                    await commentsTask,
                    await todosTask,
                    await addressesTask,
                    await countriesTask);
            }
        }

        protected static async Task<IEnumerable<T>> fetchObjects<T>(HttpClient client, string url) {
            var response = await client.GetStringAsync(url);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(response);
        }

        #region Query methods

        public User getUserById(long id) {
            if(UserSet.TryGetValue(id, out var u))
                return u;
            else
                throw new KeyNotFoundException($"User with id {id} not found!");
        }

        public Post getPostById(long id) {
            if(PostSet.TryGetValue(id, out var p))
                return p;
            else
                throw new KeyNotFoundException($"Post with id {id} not found!");
        }

        //Получить количество комментов под постами конкретного пользователя (по айди)
        //(список из пост-количество)
        public IEnumerable<(long id, string text, int count)> UserPostsCommentsCount(long userId) =>
            getUserById(userId).Posts
                .Select(p => (p.Id, p.Body, p.Comments.Count()));

        //Получить список комментов под постами конкретного пользователя (по айди),
        //где body коммента < 50 символов (список из комментов)
        public IEnumerable<(long id, string text)> ShortUserComments(long userId) =>
            getUserById(userId).Comments
                .Where(c => c.Body.Length < 50)
                .Select(c => (c.Id, c.Body));

        //Получить список (id, name) из списка todos, которые выполнены для конкретного пользователя (по айди)
        public IEnumerable<(long id, string name)> CompletedUserTodos(long userId) =>
            getUserById(userId).Todos
                .Where(t => t.IsComplete)
                .Select(t => (t.Id, t.Name));

        //Получить список пользователей по алфавиту (по возрастанию)
        //с отсортированными todo items по длине name (по убыванию)
        public IEnumerable<(string userName, string todoName)> AllUsersTodos() =>
            Users
                .SelectMany(u => u.Todos)
                .OrderBy(t => t.User.Name)
                .ThenByDescending(t => t.Name.Length)
                .Select(t => (t.User.Name, t.Name));

        //Получить следующую структуру (передать Id пользователя в параметры)
        //  User
        //  Последний пост пользователя (по дате)
        //  Количество комментов под последним постом
        //  Количество невыполненных тасков для пользователя
        //  Самый популярный пост пользователя (там где больше всего комментов с длиной текста больше 80 символов)
        //  Самый популярный пост пользователя (там где больше всего лайков)
        public UserStat UserStatistics(long UserId) {
            Func<Post, int> popularScore =
                p => p?.Comments.Where(c => c.Body.Length > 80).Count() ?? 0;

            Func<Post, Post, Post>
                mostPopular = (p1, p2) => popularScore(p1) > popularScore(p2) ? p1 : p2,
                mostLiked = (p1, p2) => (p1?.Likes ?? 0) > (p2?.Likes ?? 0) ? p1 : p2;

            var user = getUserById(UserId);

            var init = user.Posts.FirstOrDefault();

            var seed = new UserStat {
                User = user,
                LastPost = init,
                LastPostCommentsCount = init?.Comments.Count() ?? 0,
                ActiveTasksCount = user.Todos.Where(t => !t.IsComplete).Count(),
                MostPopularPost = popularScore(init) > 0 ? init : null,
                MostLikedPost = init
            };

            return user.Posts.Skip(1).Aggregate(
                seed,
                (acc, post) => {
                    if(acc.LastPost.CreatedAt < post.CreatedAt) {
                        acc.LastPost = post;
                        acc.LastPostCommentsCount = post.Comments.Count();
                    }
                    acc.MostPopularPost = mostPopular(acc.MostPopularPost, post);
                    acc.MostLikedPost = mostLiked(acc.MostLikedPost, post);

                    return acc;
                }
            );
        }

        //Получить следующую структуру (передать Id поста в параметры)
        //  Пост
        //  Самый длинный коммент поста
        //  Самый залайканный коммент поста
        //  Количество комментов под постом где или 0 лайков или длина текста < 80
        public PostStat PostStatistics(long postId) {
            Func<Comment, int> shortOrUnlikedScore = c => c.Likes == 0 || c.Body.Length < 80 ? 1 : 0;
            Func<Comment, Comment, Comment>
                longest   = (c1, c2) => c1.Body.Length > c2.Body.Length ?  c1 : c2,
                mostLiked = (c1, c2) => c1.Likes > c2.Likes ? c1 : c2;

            var post = getPostById(postId);

            var init = post.Comments.FirstOrDefault();

            var seed = new PostStat {
                LongestComment = init,
                MostLikedComment = init,
                BadOrShortCommentCount = shortOrUnlikedScore(init),
                Post = post
            };

            return post.Comments.Skip(1).Aggregate(
                seed,
                (acc, comment) => {
                    acc.MostLikedComment = mostLiked(acc.MostLikedComment, comment);
                    acc.LongestComment = longest(acc.LongestComment, comment);
                    acc.BadOrShortCommentCount += shortOrUnlikedScore(comment);
                    return acc;
                }
            );
        }
        #endregion
  }

}
