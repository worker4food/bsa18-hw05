using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models {
    public partial class User
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("avatar")]
        public string Avatar { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        public IEnumerable<Post> Posts { get; private set; }
        public IEnumerable<Comment> Comments { get; private set; }
        public IEnumerable<ToDo> Todos {get; private set; }
        public Address Address;

        public User withPosts(IEnumerable<Post> ps) {
            Posts = ps;
            return this;
        }

        public User withComments(IEnumerable<Comment> cs) {
            Comments = cs;
            return this;
        }

        public User withTodos(IEnumerable<ToDo> ts) {
            Todos = ts;
            return this;
        }

        public User witAddress(Address a) {
            Address = a;
            return this;
        }
    }
}
