using Newtonsoft.Json;

namespace Models
{
    public class Country {
        [JsonProperty("country-code")]
        public long Id { get; private set; }

        [JsonProperty("alpha-2")]
        public string Code { get; private set; }

        [JsonProperty("name")]
        public string Name { get; private set; }
    }
}
