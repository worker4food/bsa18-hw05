using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models {
    public partial class Comment
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("body")]
        public string Body { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        [JsonProperty("postId")]
        public long PostId { get; set; }

        [JsonProperty("likes")]
        public long Likes { get; set; }

        public User User { get; private set; }
        public Post Post { get; private set; }

        public Comment withUser(User u) {
            User = u;
            return this;
        }

        public Comment withPost(Post p) {
            Post = p;
            return this;
        }
    }
}
