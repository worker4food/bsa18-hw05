using System.Collections.Generic;

namespace Models {
    public class Dashboard {
        public IEnumerable<(User User, long Likes)> TopUsers { get; set; }
        public IEnumerable<Post> TopPosts { get; set; }
        public IEnumerable<ToDo> RecentTodos { get; set; }
    }
}
