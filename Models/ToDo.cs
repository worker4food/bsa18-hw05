using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models {
    public partial class ToDo
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("createdAt")]
        public DateTimeOffset CreatedAt { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isComplete")]
        public bool IsComplete { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        public User User { get; private set; }

        public ToDo withUser(User u) {
            User = u;
            return this;
        }
    }
}
