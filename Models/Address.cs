using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Models {
    public partial class Address
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }

        [JsonProperty("userId")]
        public long UserId { get; set; }

        public User User { get; private set; }

        public Address withUser(User u) {
            User = u;
            return this;
        }
    }
}
